## jump server 安装部署

#### 配置清单

|  主机名称  |    IP地址     |   最低配置   |  软件名称  |  版本   |
| :--------: | :-----------: | :----------: | :--------: | :-----: |
| jumpserver | 192.168.1.251 | 2CPU，4G内存 | jumpserver | v2.10.2 |

#### 安装部署

下载软件，部署基础环境

```shell
[root@jump-server ~]# curl -sSOL https://github.com/jumpserver/installer/releases/download/v2.10.2/jumpserver-installer-v2.10.2.tar.gz
[root@jump-server ~]# curl -sSL https://get.daocloud.io/docker/compose/releases/download/1.27.4/docker-compose-Linux-x86_64 -o docker-compose
[root@jump-server ~]# md5sum docker-compose jumpserver-installer-v2.10.2.tar.gz 
bec660213f97d788d129410d047f261f  docker-compose
223415d3cd9777a58fc0dc71c0b579cf  jumpserver-installer-v2.10.2.tar.gz
[root@jump-server ~]# yum install -y curl wget zip python firewalld
[root@jump-server ~]# # 拷贝 docker rpm 软件包到这里
[root@jump-server ~]# yum install -y ./docker-ce-18.06.3.ce-3.el7.x86_64.rpm
[root@jump-server ~]# systemctl enable --now docker firewalld
[root@jump-server ~]# cp docker-compose /usr/bin/
[root@jump-server ~]# chmod 755 /usr/bin/docker-compose
```

下载镜像


```shell
[root@jump-server ~]# DOCKER_IMAGE_PREFIX=swr.cn-north-1.myhuaweicloud.com
[root@jump-server ~]# for i in jumpserver/redis:6-alpine jumpserver/mysql:5 jumpserver/nginx:alpine2 jumpserver/luna:v2.10.2 jumpserver/core:v2.10.2 jumpserver/koko:v2.10.2 jumpserver/lion:v2.10.2 jumpserver/lina:v2.10.2;do
    docker pull ${i}; 
done
[root@jump-server ~]# tar zxf jumpserver-installer-v2.10.2.tar.gz -C /opt/
[root@jump-server ~]# cd /opt/jumpserver-installer-v2.10.2/
[root@jump-server jumpserver-installer-v2.10.2]# vim static.env
export VERSION="v2.10.2"
[root@jump-server jumpserver-installer-v2.10.2]# ./jmsctl.sh install
语言 Language  (cn/en)  (default cn): 

1. 检查配置文件
配置文件位置: /opt/jumpserver/config
/opt/jumpserver/config/config.txt  [ √ ]
/opt/jumpserver/config/nginx/lb_ssh_server.conf  [ √ ]
完成

2. 配置 Nginx
配置文件: /opt/jumpserver/config/nginx/cert
/opt/jumpserver/config/nginx/cert/server.crt  [ √ ]
/opt/jumpserver/config/nginx/cert/server.key  [ √ ]
完成

3. 备份配置文件
备份至 /opt/jumpserver/config/backup/config.txt.2021-05-27_20-09-29
完成

>>> 安装配置 Docker
1. 安装 Docker
完成

2. 配置 Docker
是否需要自定义 docker 存储目录, 默认将使用目录 /var/lib/docker? (y/n)  (默认为 n): 
完成

3. 启动 Docker
完成

>>> 加载 Docker 镜像
[jumpserver/redis:6-alpine]
[jumpserver/mysql:5]
[jumpserver/nginx:alpine2]
[jumpserver/luna:v2.10.2]
[jumpserver/core:v2.10.2]
[jumpserver/koko:v2.10.2]
[jumpserver/lion:v2.10.2]
[jumpserver/lina:v2.10.2]

>>> 安装配置 JumpServer
1. 配置网络
是否需要支持 IPv6? (y/n)  (默认为 n): 
完成

2. 配置加密密钥
SECRETE_KEY:     ZTAwOWYzNDctMjA1ZS00NzM4LThlZDMtYjEwYmY3NDJkZjA4
BOOTSTRAP_TOKEN: ZTAwOWYzNDctMjA1
完成

3. 配置持久化目录
是否需要自定义持久化存储, 默认将使用目录 /opt/jumpserver? (y/n)  (默认为 n): 
完成

4. 配置 MySQL
是否使用外部 MySQL? (y/n)  (默认为 n): 
完成

5. 配置 Redis
是否使用外部 Redis? (y/n)  (默认为 n): 
完成

>>> 安装完成了
1. 可以使用如下命令启动, 然后访问
./jmsctl.sh start

2. 其它一些管理命令
./jmsctl.sh stop
./jmsctl.sh restart
./jmsctl.sh backup
./jmsctl.sh upgrade
更多还有一些命令, 你可以 ./jmsctl.sh --help 来了解

3. Web 访问
http://192.168.1.251:8080
https://192.168.1.251:8443
默认用户: admin  默认密码: admin

4. SSH/SFTP 访问
ssh admin@192.168.1.251 -p2222
sftp -P2222 admin@192.168.1.251

5. 更多信息
我们的官网: https://www.jumpserver.org/
我们的文档: https://docs.jumpserver.org/

[root@jump-server jumpserver-installer-v2.10.2]# systemctl restart docker
[root@jump-server jumpserver-installer-v2.10.2]# ./jmsctl.sh start
Creating network "jms_net" with driver "bridge"
Creating jms_redis  ... done
Creating jms_mysql  ... done
Creating jms_core   ... done
Creating jms_lina   ... done
Creating jms_lion   ... done
Creating jms_koko   ... done
Creating jms_celery ... done
Creating jms_luna   ... done
Creating jms_nginx  ... done
[root@jump-server jumpserver-installer-v2.10.2]# ./jmsctl.sh status
Name           Command                    State          Ports
---------------------------------------------------------------------------
jms_celery    ./entrypoint.sh start task  Up (healthy)   8070/tcp, 8080/tcp
jms_core      ./entrypoint.sh start web   Up (healthy)   8070/tcp, 8080/tcp
jms_koko      ./entrypoint.sh             Up (healthy)   0.0.0.0:2222->2222/tcp, 5000/tcp
jms_lina      /docker-entrypoint.sh ngin  Up (healthy)   80/tcp
jms_lion      /usr/bin/supervisord        Up (healthy)   4822/tcp
jms_luna      /docker-entrypoint.sh ngin  Up (healthy)   80/tcp
jms_mysql     docker-entrypoint.sh --cha  Up (healthy)   3306/tcp, 33060/tcp
jms_nginx     sh -c crond -b -d 8 && ngi  Up (healthy)   0.0.0.0:8443->443/tcp, 0.0.0.0:8080->80/tcp
jms_redis     docker-entrypoint.sh redis  Up (healthy)   6379/tcp
[root@jump-server jumpserver-installer-v2.10.2]# 
```

